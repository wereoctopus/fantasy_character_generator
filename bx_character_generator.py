from random import choice, sample, seed, randint
from tables import dndBX, general, mazerats, medievalnames, neuralnetworknames
seed()

NUMBER_OF_CHARACTERS = 1000
OUTPUT_FILENAME = '{0} BX characters.txt'.format(NUMBER_OF_CHARACTERS)

# comment out all but the name list you want to use
# NAME_LIST = list(medievalnames.MEDIEVAL_EUROPEAN_NAMES)
NAME_LIST = list(neuralnetworknames.NEURAL_NETWORK_NAMES)

ROLLING_METHOD = '3d6' # supports 'array' '4d6L1', '3d6', '3d6H2+10'
ROLL_IN_ORDER = True
REROLL_SUBPAR_SCORES = False #if True, rerolls if all scores <= 8 or any score <= 3

# use these if you want to constrain something
OVERRIDE_CLASS = None
OVERRIDE_LEVEL = None #supports None, 'Random', or an integer
OVERRIDE_STR = None
OVERRIDE_DEX = None
OVERRIDE_CON = None
OVERRIDE_INT = None
OVERRIDE_WIS = None
OVERRIDE_CHA = None

SHOW_CLASS_FEATURES = 'Thief' # supports False, 'All', 'Thief'
SHOW_FLUFF_TRAITS = True
SHOW_INITIATIVE_ADJUSTMENT = False #individual initiative is a variant rule
SHOW_SPELLS = True





pc_fluff_traits = [] #todo: customise based on what's in this rather than what's below
assert (NUMBER_OF_CHARACTERS <= len(NAME_LIST)) #make fewer characters or use a longer name list
outputFile = open(OUTPUT_FILENAME, 'w')
for character in range(NUMBER_OF_CHARACTERS):
    pc_name = choice(NAME_LIST)
    NAME_LIST.remove(pc_name)

    #construct an ability score dictionary, including rerolling bad scores if indicated above
    rolled_scores = general.rollAbilityScores(dndBX.NUMBER_OF_SCORES, ROLLING_METHOD)
    bad_scores = REROLL_SUBPAR_SCORES
    while bad_scores:
        if dndBX.checkIfSubpar(rolled_scores, single_score_threshold=3, all_scores_threshold=8) == False:
            bad_scores = False
        else:
            rolled_scores = general.rollAbilityScores(dndBX.NUMBER_OF_SCORES, ROLLING_METHOD)
    ability_scores = dndBX.makeAbilityScoreDictionary(rolled_scores)

    if OVERRIDE_STR:
        ability_scores['Str'] = OVERRIDE_STR
    if OVERRIDE_DEX:
        ability_scores['Dex'] = OVERRIDE_DEX
    if OVERRIDE_CON:
        ability_scores['Con'] = OVERRIDE_CON
    if OVERRIDE_INT:
        ability_scores['Int'] = OVERRIDE_INT
    if OVERRIDE_WIS:
        ability_scores['Wis'] = OVERRIDE_WIS
    if OVERRIDE_CHA:
        ability_scores['Cha'] = OVERRIDE_CHA

    if OVERRIDE_CLASS:
        pc_class = OVERRIDE_CLASS
    else:
        pc_class = dndBX.selectClass(ability_scores)

    if OVERRIDE_LEVEL == 'Random':
        character_level = randint(1, dndBX.CLASS_ATTRIBUTES[pc_class]['Max Level'])
    elif OVERRIDE_LEVEL:
        character_level = OVERRIDE_LEVEL
    else:
        character_level = 1

    xp_bonus = dndBX.calculateXPBonus(pc_class, ability_scores)
    thac0 = dndBX.getThac0(pc_class, character_level)
    saves = dndBX.getSaves(pc_class, character_level)
    magic_save_adjustment = dndBX.getStandardAdjustment(ability_scores['Wis'])
    languages = dndBX.rollLanguages(pc_class, ability_scores)
    reaction_adjustment = dndBX.getReactionAdjustment(ability_scores['Cha'])
    retainers = dndBX.getRetainerNumberAndMorale(ability_scores['Cha'])
    #returns None if class/level would have no spells, e.g. dwarf, or cleric 1
    pc_spells = dndBX.rollSpells(pc_class, character_level)


    if pc_class == 'Thief':
        thief_skills = dndBX.THIEF_SKILLS[character_level]
    else:
        thief_skills = None

    pc_fluff_traits.clear()
    pc_fluff_traits.extend([
        "Alignment: " + choice(dndBX.ALIGNMENT),
        "Appearance: " + choice(mazerats.APPEARANCE),
        "Physical Detail: " + choice(mazerats.PHYSICAL_DETAIL),
        "Clothing: " + choice(mazerats.CLOTHING),
        "Personality: " + choice(mazerats.PERSONALITY),
        "Mannerism: " + choice(mazerats.MANNERISM)
    ])

    statblock = str(character + 1) + '.\n'          \
              + pc_name + '\n'                      \
              + pc_class + ' ' + str(character_level) + '\n'

    statblock += 'HP: ' + str(dndBX.rollHP(pc_class, ability_scores, character_level)) +'\n'

    for score in dndBX.ABILITY_SCORES:
        statblock += score + ' ' + str(ability_scores[score]) + '  '
    statblock += '\n'

    statblock += 'XP Bonus: ' + str(xp_bonus) + '%\n'

    if SHOW_INITIATIVE_ADJUSTMENT:
        initiative = dndBX.getInitiativeAdjustment(ability_scores['Dex'])
        if initiative >= 0:
            statblock += 'Initiative: +' + str(initiative) + '\n'
        else:
            statblock += 'Initiative: ' + str(initiative) + '\n'

    statblock += 'THAC0: ' + str(thac0) + '\n'

    statblock += 'Saves:\n'
    for save in dndBX.SAVING_THROW_ORDER:
        statblock += save + ' ' + str(saves[save]) + '  '
    statblock += '\n'
    if magic_save_adjustment != 0:
        if magic_save_adjustment > 0:
            statblock += '+'
        statblock += '{0} to saves vs magical effects\n'.format(magic_save_adjustment)

    statblock += 'Languages: ' + ', '.join(languages[1]) + '\n' + languages[0] + '\n'
    if reaction_adjustment >= 0:
        reaction_adjustment = '+{0}'.format(reaction_adjustment)
    statblock += 'Reaction: {0}\n'.format(reaction_adjustment)
    statblock += 'Maximum Retainers: {0} (Morale: {1})\n'.format(retainers[0], retainers[1])

    if SHOW_CLASS_FEATURES == 'All':
        statblock += 'Abilities:\n'
        for level in range(character_level):
            if dndBX.CLASS_FEATURES[pc_class][level]:
                statblock += '\n'.join(dndBX.CLASS_FEATURES[pc_class][level]) + '\n'

    if SHOW_CLASS_FEATURES in ('All', 'Thief'):
        if thief_skills:
            statblock += 'Thief Skills:\n'
            for skill in dndBX.THIEF_SKILL_ORDER:
                statblock += skill + ':  ' + thief_skills[skill] + '\n'

    if SHOW_SPELLS:
        if pc_spells:
            statblock += 'Spells:\n'
            for spells_of_a_given_level in pc_spells:
                if spells_of_a_given_level:
                    statblock += ', '.join(spells_of_a_given_level) + '\n'

    if SHOW_FLUFF_TRAITS:
        for trait in pc_fluff_traits:
            statblock += trait + '\n'

    statblock += '\n'

    outputFile.write(statblock)
outputFile.close()
