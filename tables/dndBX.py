from random import choice, randint, sample

ABILITY_SCORES = ('Str', 'Int', 'Wis', 'Dex', 'Con', 'Cha')
NUMBER_OF_SCORES = len(ABILITY_SCORES)
# nested dictionaries
#saving throw tables defined elsewhere, this is so custom classes can easily refer to one of the standard tables
CLASS_ATTRIBUTES = {
    'Cleric':       {'Prime Requisite': ('Wis',),
                     'Requirements': None,
                     'Hit Die': 6,
                     'Max Level': 14,
                     'Max Hit Dice': 9,
                     'Subsequent HP': 1,
                     'Armor': 'Any, including shields',
                     'Weapons': 'Any blunt weapons',
                     'Languages': ('Common',),
                     'Attacks as': 'Cleric',
                     'Saves as': 'Cleric',
                     'Spell List': 'Cleric',
                     'Spells/Day': 'Cleric',
                    },

    'Dwarf':        {'Prime Requisite': ('Str',),
                     'Requirements': (('Con', 9),),
                     'Hit Die': 8,
                     'Max Level': 12,
                     'Max Hit Dice': 9,
                     'Subsequent HP': 3,
                     'Armor': 'Any, including shields',
                     'Weapons': 'Any small or normal-sized',
                     'Languages': ('Common', 'Dwarvish', 'Gnome', 'Goblin', 'Kobold',),
                     'Attacks as': 'Fighter',
                     'Saves as': 'Dwarf',
                     'Spell List': None,
                     'Spells/Day': None,
                    },

    'Elf':          {'Prime Requisite': ('Str', 'Int'),
                     'Requirements': (('Int', 9),),
                     'Hit Die': 6,
                     'Max Level': 10,
                     'Max Hit Dice': 9,
                     'Subsequent HP': 2,
                     'Armor': 'Any, including shields',
                     'Weapons': 'Any',
                     'Languages': ('Common', 'Elvish', 'Gnoll', 'Hobgoblin', 'Orcish',),
                     'Attacks as': 'Fighter',
                     'Saves as': 'Elf',
                     'Spell List': 'Magic-User',
                     'Spells/Day': 'Elf',
                    },

    'Fighter':      {'Prime Requisite': ('Str',),
                     'Requirements': None,
                     'Hit Die': 8,
                     'Max Level': 14,
                     'Max Hit Dice': 9,
                     'Subsequent HP': 2,
                     'Armor': 'Any, including shields',
                     'Weapons': 'Any',
                     'Languages': ('Common',),
                     'Attacks as': 'Fighter',
                     'Saves as': 'Fighter',
                     'Spell List': None,
                     'Spells/Day': None,
                    },

    'Halfling':     {'Prime Requisite': ('Str', 'Dex'),
                     'Requirements': (('Dex', 9), ('Con', 9)),
                     'Hit Die': 6,
                     'Max Level': 8,
                     'Max Hit Dice': 8,
                     'Subsequent HP': 0,
                     'Armor': 'Any appropriate to size, including shields',
                     'Weapons': 'Any small or normal-sized',
                     'Languages': ('Common', 'Halfling'),
                     'Attacks as': 'Fighter',
                     'Saves as': 'Dwarf',
                     'Spell List': None,
                     'Spells/Day': None,
                    },

    'Magic-User':   {'Prime Requisite': ('Int',),
                     'Requirements': None,
                     'Hit Die': 4,
                     'Max Level': 14,
                     'Max Hit Dice': 9,
                     'Subsequent HP': 1,
                     'Armor': 'None',
                     'Weapons': 'Dagger only',
                     'Languages': ('Common',),
                     'Attacks as': 'Magic-User',
                     'Saves as': 'Magic-User',
                     'Spell List': 'Magic-User',
                     'Spells/Day': 'Magic-User',
                    },

    'Thief':        {'Prime Requisite': ('Dex',),
                     'Requirements': None,
                     'Hit Die': 4,
                     'Max Level': 14,
                     'Max Hit Dice': 9,
                     'Subsequent HP': 2,
                     'Armor': 'Leather, no shields',
                     'Weapons': 'Any',
                     'Languages': ('Common',),
                     'Attacks as': 'Cleric',
                     'Saves as': 'Thief',
                     'Spell List': None,
                     'Spells/Day': None,
                    }
}
# defined separately largely to make CLASS_ATTRIBUTES more readable/hackable
# Intention is that if a custom class is inserted into CLASS_ATTRIBUTES but not
# into here e.g. because its abilities are long and complicated to code,
# bx_character_generator.py will still run.

CLASS_FEATURES = {
    'Cleric': (
        ('Holy Symbol', 'Deity Disfavour', 'Spell Research', 'Turning the Undead', 'Using Magic Items',),
        ('Spell Casting',),
        (),
        (),
        (),

        (),
        (),
        (),
        ('Magic Item Research', 'Stronghold',),
        (),

        (),
        (),
        (),
        (),
     ),

    'Dwarf': (
        ('Detect Construction Tricks', 'Detect Traps', 'Infravision', 'Listening at Doors',),
        (),
        (),
        (),
        (),

        (),
        (),
        (),
        ('Underground Stronghold',),
        (),

        (),
        (),
     ),

    'Elf': (
        ('Detect Secret Doors', 'Immunity to Ghoul Paralysis', 'Infravision', 'Listening at Doors', 'Spell Casting', 'Spell Research', 'Using Magic Items',),
        (),
        (),
        (),
        (),

        (),
        (),
        (),
        ('Magic Item Research', 'Natural Stronghold',),
        (),
     ),

    'Fighter': (
        ('Stronghold',),
        (),
        (),
        (),
        (),

        (),
        (),
        (),
        ('Title',),
        (),

        (),
        (),
        (),
        (),
     ),

    'Halfling': (
        ('Defensive Bonus', 'Hiding', 'Initiative Bonus (optional rule)', 'Listening at Doors', 'Missile Attack Bonus', 'Stronghold',),
        (),
        (),
        (),
        (),

        (),
        (),
        (),
     ),

    'Magic-User': (
        ('Spell Casting', 'Spell Research',),
        (),
        (),
        (),
        (),

        (),
        (),
        (),
        ('Magic Item Research',),
        (),

        ('Wizard\'s Tower',),
        (),
        (),
        (),
     ),

    'Thief': (
        ('Back-stab', 'Thief Skills',),
        (),
        (),
        ('Read Languages',),
        (),

        (),
        (),
        (),
        ('Thief Den',),
        ('Scroll Use',),

        (),
        (),
        (),
        (),
     ),
}
# 4-in-6 probability of being Neutral
ALIGNMENT = (
    'Lawful',
    'Neutral',
    'Neutral',
    'Neutral',
    'Neutral',
    'Chaotic'
)
# keys of inner dictionaries are the level ranges (inclusive of both ends)
# that correspond to a thac0 value
# e.g. an 8th level Thief attacks as a Cleric in the (5, 8) bracket, hence
# has a thac0 of 17
# you would retrieve this with ATTACK_CHARTS['Cleric'][(5, 8)]
# or anything that evaluates to that. See getThac0()
ATTACK_CHARTS = {
    'Fighter': {
        (1, 3):   19,
        (4, 6):   17,
        (7, 9):   14,
        (10, 12): 12,
        (13, 15): 10,
    },

    'Cleric': {
        (1, 4):   19,
        (5, 8):   17,
        (9, 12):  14,
        (13, 16): 12,
        (17, 20): 10,
    },

    'Magic-User': {
        (1, 5):   19,
        (6, 10):  17,
        (11, 15): 14,
        (16, 20): 12,
        (21, 25): 10,
    },
}

LANGUAGES = (
    'Bugbear',
    'Doppelgänger',
    'Dragon',
    'Dwarvish',
    'Elvish',

    'Gargoyle',
    'Gnoll',
    'Gnome'
    'Goblin',
    'Halfling',

    'Harpy',
    'Hobgoblin',
    'Kobold',
    'Lizard man',
    'Medusa',

    'Minotaur',
    'Ogre',
    'Orc',
    'Pixie',
    'Human dialect',
)

SAVING_THROW_ORDER = ('Death', 'Wands', 'Paralysis', 'Breath', 'Spells')

SAVE_CHARTS = {
    'Cleric': {
        (1, 4):   {'Death': 11, 'Wands': 12, 'Paralysis': 14, 'Breath': 16, 'Spells': 15,},
        (5, 8):   {'Death': 9,  'Wands': 10, 'Paralysis': 12, 'Breath': 14, 'Spells': 12,},
        (9, 12):  {'Death': 6,  'Wands': 7,  'Paralysis': 9,  'Breath': 11, 'Spells': 9,},
        (13, 16): {'Death': 3,  'Wands': 5,  'Paralysis': 7,  'Breath': 8,  'Spells': 7,},
    },

    'Dwarf': {
        (1, 3):   {'Death': 8,  'Wands': 9,  'Paralysis': 10, 'Breath': 13, 'Spells': 12,},
        (4, 6):   {'Death': 6,  'Wands': 7,  'Paralysis': 8,  'Breath': 10, 'Spells': 10,},
        (7, 9):   {'Death': 4,  'Wands': 5,  'Paralysis': 6,  'Breath': 7,  'Spells': 8,},
        (10, 12): {'Death': 2,  'Wands': 3,  'Paralysis': 4,  'Breath': 4,  'Spells': 6,},
    },

    'Elf': {
        (1, 3):   {'Death': 12, 'Wands': 13, 'Paralysis': 13, 'Breath': 15, 'Spells': 15,},
        (4, 6):   {'Death': 10, 'Wands': 11, 'Paralysis': 11, 'Breath': 13, 'Spells': 12,},
        (7, 9):   {'Death': 8,  'Wands': 9,  'Paralysis': 9,  'Breath': 10, 'Spells': 10,},
        (10, 10): {'Death': 6,  'Wands': 7,  'Paralysis': 8,  'Breath': 8,  'Spells': 8,},
    },

    'Fighter': {
        (1, 3):   {'Death': 12, 'Wands': 13, 'Paralysis': 14, 'Breath': 15, 'Spells': 16,},
        (4, 6):   {'Death': 10, 'Wands': 11, 'Paralysis': 12, 'Breath': 13, 'Spells': 14,},
        (7, 9):   {'Death': 8,  'Wands': 9,  'Paralysis': 10, 'Breath': 10, 'Spells': 12,},
        (10, 12): {'Death': 6,  'Wands': 7,  'Paralysis': 8,  'Breath': 8,  'Spells': 10,},
        (13, 15): {'Death': 4,  'Wands': 5,  'Paralysis': 6,  'Breath': 5,  'Spells': 8,},
    },

    'Magic-User': {
        (1, 5):   {'Death': 13, 'Wands': 14, 'Paralysis': 13, 'Breath': 16, 'Spells': 15,},
        (6, 10):  {'Death': 11, 'Wands': 12, 'Paralysis': 11, 'Breath': 14, 'Spells': 12,},
        (11, 15): {'Death': 8,  'Wands': 9,  'Paralysis': 8,  'Breath': 11, 'Spells': 8,},
    },

    'Thief': {
        (1, 4):   {'Death': 13, 'Wands': 14, 'Paralysis': 13, 'Breath': 16, 'Spells': 15,},
        (5, 8):   {'Death': 12, 'Wands': 13, 'Paralysis': 11, 'Breath': 14, 'Spells': 13,},
        (9, 12):  {'Death': 10, 'Wands': 11, 'Paralysis': 9,  'Breath': 12, 'Spells': 10,},
        (13, 16): {'Death': 8,  'Wands': 9,  'Paralysis': 7,  'Breath': 10, 'Spells': 8,},
    },
}
# vanilla BX only, i.e. no Labyrinth Lord Advanced Edition Companion spells.
# BX has no cantrips, to make indexing spell level
#easier/readable/cross-compatible an empty tuple represents 0th level spells.
SPELL_DICTIONARY = {
    'Cleric':       (
                       (),

                       ('cure light wounds (cause light wounds)',
                        'detect evil',
                        'detect magic',
                        'light (darkness)',
                        'protection from evil',
                        'purify food and water',
                        'remove fear (cause fear)',
                        'resist cold',
                        ),

                       ('bless (blight)',
                        'find traps',
                        'hold person',
                        'know alignment',
                        'resist fire',
                        'silence 15\' radius',
                        'snake charm',
                        'speak with animals',
                        ),

                       ('continual light (continual darkness)',
                        'cure disease (cause disease)',
                        'growth of animal',
                        'locate object',
                        'remove curse (curse)',
                        'striking',
                        ),

                       ('create water',
                        'cure serious wounds (cause serious wounds)',
                        'neutralize poison',
                        'protection from evil 10\' radius',
                        'speak with plants',
                        'sticks to snakes',
                        ),

                       ('commune',
                        'create food',
                        'dispel evil',
                        'insect plague',
                        'quest (remove quest)',
                        'raise dead (finger of death)',
                        )
                    ),


    'Magic-User':  (
                       (),

                       ('charm person',
                        'detect magic',
                        'floating disc',
                        'hold portal',
                        'light (darkness)',
                        'magic missile',
                        'protection from evil',
                        'read languages',
                        'read magic',
                        'shield',
                        'sleep',
                        'ventriloquism',
                        ),

                       ('continual light (continual darkness)',
                        'detect evil',
                        'detect invisible',
                        'ESP',
                        'invisibility',
                        'knock',
                        'levitate',
                        'locate object',
                        'mirror image',
                        'phantasmal force',
                        'web',
                        'wizard lock',
                        ),

                       ('clairvoyance',
                        'dispel magic',
                        'fire ball',
                        'fly',
                        'haste',
                        'hold person',
                        'infravision',
                        'invisibility 10\' radius',
                        'lightning bolt',
                        'protection from evil 10\' radius',
                        'protection from normal missiles',
                        'water breathing',
                        ),

                       ('charm monster',
                        'confusion',
                        'dimension door',
                        'growth of plants',
                        'hallucinatory terrain',
                        'massmorph',
                        'polymorph others',
                        'polymorph self',
                        'remove curse (curse)',
                        'wall of fire',
                        'wall of ice',
                        'wizard eye',
                        ),

                       ('animate dead',
                        'cloudkill',
                        'conjure elemental',
                        'contact higher plane',
                        'feeblemind',
                        'hold monster',
                        'magic jar',
                        'pass-wall',
                        'telekinesis',
                        'teleport',
                        'transmute rock to mud (transmute mud to rock)',
                        'wall of stone',
                       ),

                       ('anti-magic shell',
                        'control weather',
                        'death spell',
                        'disintegrate',
                        'geas (remove geas)',
                        'invisible stalker',
                        'lower water',
                        'move earth',
                        'part water',
                        'projected image',
                        'reincarnation',
                        'stone to flesh (flesh to stone)',
                       )
                    )
}

SPELLS_PER_DAY = {
    'Cleric':       (
        (),

        (0, 0, 0, 0, 0, 0),
        (0, 1, 0, 0, 0, 0),
        (0, 2, 0, 0, 0, 0),
        (0, 2, 1, 0, 0, 0),
        (0, 2, 2, 0, 0, 0),

        (0, 2, 2, 1, 1, 0),
        (0, 2, 2, 2, 1, 1),
        (0, 3, 3, 2, 2, 1),
        (0, 3, 3, 3, 2, 2),
        (0, 4, 4, 3, 3, 2),

        (0, 4, 4, 4, 3, 3),
        (0, 5, 5, 4, 4, 3),
        (0, 5, 5, 5, 4, 4),
        (0, 6, 5, 5, 5, 4),
        ),

    'Elf':          (
        (),

        (0, 1, 0, 0, 0, 0),
        (0, 2, 0, 0, 0, 0),
        (0, 2, 1, 0, 0, 0),
        (0, 2, 2, 0, 0, 0),
        (0, 2, 2, 1, 0, 0),

        (0, 2, 2, 2, 0, 0),
        (0, 3, 2, 2, 1, 0),
        (0, 3, 3, 2, 2, 0),
        (0, 3, 3, 3, 2, 1),
        (0, 3, 3, 3, 3, 2),
        ),

    'Magic-User':   (
        (),

        (0, 1, 0, 0, 0, 0, 0),
        (0, 2, 0, 0, 0, 0, 0),
        (0, 2, 1, 0, 0, 0, 0),
        (0, 2, 2, 0, 0, 0, 0),
        (0, 2, 2, 1, 0, 0, 0),

        (0, 2, 2, 2, 0, 0, 0),
        (0, 3, 2, 2, 1, 0, 0),
        (0, 3, 3, 2, 2, 0, 0),
        (0, 3, 3, 3, 2, 1, 0),
        (0, 3, 3, 3, 3, 2, 0),

        (0, 4, 3, 3, 3, 2, 1),
        (0, 4, 4, 4, 3, 3, 2),
        (0, 4, 4, 4, 3, 3, 3),
        (0, 4, 4, 4, 4, 3, 3),
        ),
}

THIEF_SKILL_ORDER = ('Climb Sheer Surfaces', 'Find or Remove Traps', \
'Hear Noise', 'Hide in Shadows', 'Move Silently', 'Pick Locks', 'Pick Pockets')

THIEF_SKILLS = (
    # empty dictionary makes it easier to look up the correct entry with THIEF_SKILLS[level]
    {},
    # 1
    {'Climb Sheer Surfaces':'87%', 'Find or Remove Traps':'10%', 'Hear Noise':'2-in-6', \
    'Hide in Shadows':'10%', 'Move Silently':'20%', 'Pick Locks':'15%', 'Pick Pockets':'20%'},
    # 2
    {'Climb Sheer Surfaces':'88%', 'Find or Remove Traps':'15%', 'Hear Noise':'2-in-6', \
    'Hide in Shadows':'15%', 'Move Silently':'25%', 'Pick Locks':'20%', 'Pick Pockets':'25%'},
    # 3
    {'Climb Sheer Surfaces':'89%', 'Find or Remove Traps':'20%', 'Hear Noise':'3-in-6', \
    'Hide in Shadows':'20%', 'Move Silently':'30%', 'Pick Locks':'25%', 'Pick Pockets':'30%'},
    # 4
    {'Climb Sheer Surfaces':'90%', 'Find or Remove Traps':'25%', 'Hear Noise':'3-in-6', \
    'Hide in Shadows':'25%', 'Move Silently':'35%', 'Pick Locks':'30%', 'Pick Pockets':'35%'},
    # 5
    {'Climb Sheer Surfaces':'91%', 'Find or Remove Traps':'30%', 'Hear Noise':'3-in-6', \
    'Hide in Shadows':'30%', 'Move Silently':'40%', 'Pick Locks':'35%', 'Pick Pockets':'40%'},
    # 6
    {'Climb Sheer Surfaces':'92%', 'Find or Remove Traps':'40%', 'Hear Noise':'3-in-6', \
    'Hide in Shadows':'36%', 'Move Silently':'45%', 'Pick Locks':'45%', 'Pick Pockets':'45%'},
    # 7
    {'Climb Sheer Surfaces':'93%', 'Find or Remove Traps':'50%', 'Hear Noise':'4-in-6', \
    'Hide in Shadows':'45%', 'Move Silently':'55%', 'Pick Locks':'55%', 'Pick Pockets':'55%'},
    # 8
    {'Climb Sheer Surfaces':'94%', 'Find or Remove Traps':'60%', 'Hear Noise':'4-in-6', \
    'Hide in Shadows':'55%', 'Move Silently':'65%', 'Pick Locks':'65%', 'Pick Pockets':'65%'},
    # 9
    {'Climb Sheer Surfaces':'95%', 'Find or Remove Traps':'70%', 'Hear Noise':'4-in-6', \
    'Hide in Shadows':'65%', 'Move Silently':'75%', 'Pick Locks':'75%', 'Pick Pockets':'75%'},
    # 10
    {'Climb Sheer Surfaces':'96%', 'Find or Remove Traps':'80%', 'Hear Noise':'4-in-6', \
    'Hide in Shadows':'75%', 'Move Silently':'85%', 'Pick Locks':'85%', 'Pick Pockets':'85%'},
    # 11
    {'Climb Sheer Surfaces':'97%', 'Find or Remove Traps':'90%', 'Hear Noise':'5-in-6', \
    'Hide in Shadows':'85%', 'Move Silently':'95%', 'Pick Locks':'95%', 'Pick Pockets':'95%'},
    # 12
    {'Climb Sheer Surfaces':'98%', 'Find or Remove Traps':'95%', 'Hear Noise':'5-in-6', \
    'Hide in Shadows':'90%', 'Move Silently':'96%', 'Pick Locks':'96%', 'Pick Pockets':'105%'},
    # 13
    {'Climb Sheer Surfaces':'99%', 'Find or Remove Traps':'97%', 'Hear Noise':'5-in-6', \
    'Hide in Shadows':'95%', 'Move Silently':'98%', 'Pick Locks':'97%', 'Pick Pockets':'115%'},
    # 14
    {'Climb Sheer Surfaces':'99%', 'Find or Remove Traps':'99%', 'Hear Noise':'5-in-6', \
    'Hide in Shadows':'99%', 'Move Silently':'99%', 'Pick Locks':'99%', 'Pick Pockets':'125%'},
)

def calculateXPBonus(class_to_check, ability_scores):
    prime_requisites = CLASS_ATTRIBUTES[class_to_check]['Prime Requisite']
    if len(prime_requisites) == 1:
        if ability_scores[prime_requisites[0]] >= 16:
            XP_Bonus = 10
        elif ability_scores[prime_requisites[0]] >= 13:
            XP_Bonus = 5
        elif ability_scores[prime_requisites[0]] >= 9:
            XP_Bonus = 0
        elif ability_scores[prime_requisites[0]] >= 6:
            XP_Bonus = -10
        else:
            XP_Bonus = -20
    elif len(prime_requisites) == 2:
        XP_Bonus = 0
        for requisite in prime_requisites:
            if ability_scores[requisite] >= 13:
                XP_Bonus += 5
            elif ability_scores[requisite] >= 9:
                XP_Bonus += 0
            elif ability_scores[requisite] >= 6:
                XP_Bonus -= 10
            else:
                XP_Bonus -= 20
    else:
        raise Exception #unknown prime requisite behaviour
    return XP_Bonus

def checkAbilityScoreRequirements(class_to_check, ability_scores):
    return_value = True
    if CLASS_ATTRIBUTES[class_to_check]['Requirements']:
        for requirement in CLASS_ATTRIBUTES[class_to_check]['Requirements']:
            if ability_scores[requirement[0]] < requirement[1]:
                return_value = False
    return return_value

def checkIfSubpar(score_list, single_score_threshold=None, all_scores_threshold=None):
    return_value = False
    if single_score_threshold:
        for score in score_list:
            if score <= single_score_threshold:
                return_value = True
                break
    if all_scores_threshold:
        passes_test = False
        for score in score_list:
            if score > all_scores_threshold:
                passes_test = True
                break
        if not passes_test:
            return_value = True
    return return_value

def getThac0(pc_class, character_level):
    attacks_as = CLASS_ATTRIBUTES[pc_class]['Attacks as']
    for level_range in ATTACK_CHARTS[attacks_as].keys():
        if character_level in range(level_range[0], level_range[1] + 1):
            return ATTACK_CHARTS[attacks_as][level_range]
    raise Exception #character level outside defined attack charts

def getSaves(pc_class, character_level):
    saves_as = CLASS_ATTRIBUTES[pc_class]['Saves as']
    for level_range in SAVE_CHARTS[saves_as].keys():
        if character_level in range(level_range[0], level_range[1] + 1):
            return SAVE_CHARTS[saves_as][level_range]
    raise Exception #character level outside defined attack charts

def getInitiativeAdjustment(score):
    if score < 4:
        return -2
    elif score < 6:
        return -1
    elif score < 9:
        return -1
    elif score < 13:
        return 0
    elif score < 16:
        return 1
    elif score < 18:
        return 1
    else:
        return 2

def getReactionAdjustment(score):
    if score < 4:
        return -2
    elif score < 6:
        return -1
    elif score < 9:
        return -1
    elif score < 13:
        return 0
    elif score < 16:
        return 1
    elif score < 18:
        return 1
    else:
        return 2
# returns tuple of (str, int)
# str is a sentence detailing language ability
# int is how many bonus languages the character knows
def getLanguageAbility(score):
    if score < 4:
        return ('Unable to read or write, broken speech', 0)
    elif score < 6:
        return ('Unable to read or write', 0)
    elif score < 9:
        return ('Can write simple words', 0)
    elif score < 13:
        return ('Can read and write native languages', 0)
    elif score < 16:
        return ('Can read and write native languages', 1)
    elif score < 18:
        return ('Can read and write native languages', 2)
    else:
        return ('Can read and write native languages', 3)

def getRetainerNumberAndMorale(score):
    if score < 4:
        return (1,4)
    elif score < 6:
        return (2,5)
    elif score < 9:
        return (3,6)
    elif score < 13:
        return (4,7)
    elif score < 16:
        return (5,8)
    elif score < 18:
        return (6,9)
    else:
        return (7,10)

def getStandardAdjustment(score):
    if score < 4:
        return -3
    elif score < 6:
        return -2
    elif score < 9:
        return -1
    elif score < 13:
        return 0
    elif score < 16:
        return 1
    elif score < 18:
        return 2
    else:
        return 3

def makeAbilityScoreDictionary(ability_score_list):
    assert(len(ability_score_list) == len(ABILITY_SCORES))
    assert(isinstance(ability_score_list, list))

    score_dict = {}
    for score in ABILITY_SCORES:
        score_dict[score] = ability_score_list.pop()
    return score_dict
# given a dictionary of ability scores, chooses a PC class that would receive the
# greatest XP bonus (ties broken randomly).
# returns None if the ability scores don't meet the Requirements for *any* class
# which shouldn't happen for vanilla BX, as human classes don't have Requirements
def selectClass(ability_scores):
    best_classes = []
    best_bonus = -100 #an arbitrarily low number
    for player_class in CLASS_ATTRIBUTES.keys():
        if checkAbilityScoreRequirements(player_class, ability_scores):
            xp_bonus = calculateXPBonus(player_class, ability_scores)
            if xp_bonus > best_bonus: #it's a new record!
                best_classes.clear()
                best_classes.append(player_class)
                best_bonus = xp_bonus
            elif xp_bonus == best_bonus: #it's a tie!
                best_classes.append(player_class)
    if best_classes:
        return choice(best_classes)
    else:
        return None

def rollHP(pc_class, ability_scores, character_level):
    hit_points = 0
    con_adjustment = getStandardAdjustment(ability_scores['Con'])
    for i in range(CLASS_ATTRIBUTES[pc_class]['Max Hit Dice']):
        hit_die_roll = randint(1, CLASS_ATTRIBUTES[pc_class]['Hit Die']) + con_adjustment
        if hit_die_roll > 1:
            hit_points += hit_die_roll
        else:
            hit_points += 1
        if i + 1 == character_level:
            break
    else:
        remaining_levels = character_level - CLASS_ATTRIBUTES[pc_class]['Max Hit Dice']
        hit_points += remaining_levels * CLASS_ATTRIBUTES[pc_class]['Subsequent HP']
    return hit_points

def rollLanguages(pc_class, ability_scores):
    class_languages = list(CLASS_ATTRIBUTES[pc_class]['Languages'])
    language_ability = getLanguageAbility(ability_scores['Int'])
    if language_ability[1] > 0:
        language_list = list(LANGUAGES)
        for known_language in class_languages:
            if known_language in language_list:
                language_list.remove(known_language)
        class_languages.extend(sample(language_list, language_ability[1]))
    class_languages.sort()
    return (language_ability[0], class_languages)

def rollSpells(pc_class, character_level):
    if not CLASS_ATTRIBUTES[pc_class]['Spells/Day']:
        return None
    else:
        spell_list = CLASS_ATTRIBUTES[pc_class]['Spell List']
        spells_per_day = SPELLS_PER_DAY[CLASS_ATTRIBUTES[pc_class]['Spells/Day']][character_level]
        spells = []
        for spell_level in range(len(spells_per_day)):
            spells.append([])
            for i in range(spells_per_day[spell_level]):
                spells[spell_level].append(choice(SPELL_DICTIONARY[spell_list][spell_level]))
            spells[spell_level].sort(key=str.lower)
        return spells
