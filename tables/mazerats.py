# A set of tuples containing fantasy character traits
# for use with blah.py or other random character generators

# All entries are taken from Maze Rats v4.3, a tRPG by Ben Milton (CC BY 4.0)
# see www.questingblog.com or www.youtube.com/questingbeast

# tuple names:
# APPEARANCE PHYSICAL_DETAIL CLOTHING PERSONALITY MANNERISM

APPEARANCE = (
'Aquiline',
'Athletic',
'Barrel-Chested',
'Boney',
'Brawny',
'Brutish',
'Bullnecked',
'Chiseled',
'Coltish',
'Corpulent',
'Craggy',
'Delicate',
'Furrowed',
'Gaunt',
'Gorgeous',
'Grizzled',
'Haggard',
'Handsome',
'Hideous',
'Lanky',
'Pudgy',
'Ripped',
'Rosy',
'Scrawny',
'Sinewy',
'Slender',
'Slumped',
'Solid',
'Square-Jawed',
'Statuesque',
'Towering',
'Trim',
'Weathered',
'Willowy',
'Wiry',
'Wrinkled'
)


PHYSICAL_DETAIL = (
'Acid scars',
'Battle scars',
'Birthmark',
'Braided hair',
'Brand mark',
'Broken nose',
'Bronze skinned',
'Burn scars',
'Bushy eyebrows',
'Curly hair',
'Dark skinned',
'Dreadlocks',
'Exotic accent',
'Flogging scars',
'Freckles',
'Gold tooth',
'Hoarse voice',
'Huge beard',
'Long hair',
'Matted hair',
'Missing ear',
'Missing teeth',
'Mustache',
'Muttonchops',
'Nine fingers',
'Oiled hair',
'One-eyed',
'Pale skinned',
'Piercings',
'Ritual scars',
'Sallow skin',
'Shaved head',
'Sunburned',
'Tangled hair',
'Tattoos',
'Topknot'
)


CLOTHING = (
'Antique',
'Battle-torn',
'Bedraggled',
'Blood-stained',
'Ceremonial',
'Dated',
'Decaying',
'Eccentric',
'Elegant',
'Embroidered',
'Exotic',
'Fashionable',
'Flamboyant',
'Food-stained',
'Formal',
'Frayed',
'Frumpy',
'Garish',
'Grimy',
'Haute couture',
'Lacey',
'Livery',
'Mud-stained',
'Ostentatious',
'Oversized',
'Patched',
'Patterned',
'Perfumed',
'Practical',
'Rumpled',
'Sigils',
'Singed',
'Tasteless',
'Undersized',
'Wine-stained',
'Worn out'
)


PERSONALITY = (
'Bitter',
'Brave',
'Cautious',
'Chipper',
'Contrary',
'Cowardly',
'Cunning',
'Driven',
'Entitled',
'Gregarious',
'Grumpy',
'Heartless',
'Honor-bound',
'Hotheaded',
'Inquisitive',
'Irascible',
'Jolly',
'Know-it-all',
'Lazy',
'Loyal',
'Menacing',
'Mopey',
'Nervous',
'Protective',
'Righteous',
'Rude',
'Sarcastic',
'Savage',
'Scheming',
'Serene',
'Spacey',
'Stoic',
'Stubborn',
'Stuck-up',
'Suspicious',
'Wisecracking'
)


MANNERISM = (
'Anecdotes',
'Breathy',
'Chuckles',
'Clipped',
'Cryptic',
'Deep voice',
'Drawl',
'Enunciates',
'Flowery speech',
'Gravelly voice',
'Highly formal',
'Hypnotic',
'Interrupts',
'Laconic',
'Laughs',
'Long pauses',
'Melodious',
'Monotone',
'Mumbles',
'Narrates',
'Overly casual',
'Quaint sayings',
'Rambles',
'Random facts',
'Rapid-fire',
'Rhyming',
'Robotic',
'Slow speech',
'Speechifies',
'Squeaky',
'Street slang',
'Stutters',
'Talks to self',
'Trails off',
'Very loud',
'Whispers'
)
