from random import choice, randint, sample, shuffle
from tables import dnd5e
from math import floor

# list of functions and their arguments:
# applyRacialAdjustments(scores, pc_subrace)
# chooseSkills(pc_race, pc_background, pc_class)
# chooseSpells(pc_class, pc_subclass, character_level, spellcasting_ability_modifier)
# getFavoredEnemy()
# getNumberOfSpells(pc_class, character_level, spellcasting_ability_modifier, spellbook=False)
# getRaceAndSubrace()
# getSubclass(pc_class)
# getSpellcastingModifier(pc_class, final_scores)
# rollAbilityScores(number_of_scores, rolling_method)
#assignAbilityScores(rolled_scores, pc_class)


# LIST OF FUNCTIONS THAT NEED DICT REIMPLEMENTATION OF ABILITY SCORES
# rollAbilityScores(number_of_scores, rolling_method)
#assignAbilityScores(rolled_scores, pc_class)

def applyRacialAdjustments(scores, pc_subrace):
    #racial_adjustments is itself a dictionary
    racial_adjustments = dnd5e.RACE_ABILITY_SCORE_ADJUSTMENTS[pc_subrace]
    for ability_score in racial_adjustments.keys():
        scores[ability_score] += racial_adjustments[ability_score]
    #ugly kludge for half elf and other races that have free-floating stat-boosts
    if pc_subrace == 'Half-Elf':
        wild_score_increases = sample(('Str', 'Dex', 'Con', 'Int', 'Wis'), 2)
        for ability_score in wild_score_increases:
            scores[ability_score] += 1
    return scores

# pc_race is a 2-element list of ['race', 'subrace']
# pc_background & pc_class are strings
# Looks up automatic skill proficiencies according to race/background,
# then chooses remaining skills from class without overlap.
# returns proficiences as a sorted list.
def chooseSkills(pc_race, pc_background, pc_class, pc_subclass=None):
    #first get the background proficiencies.
    #convert to lists since the data is stored as immutable tuples.
    pc_proficiencies = list(dnd5e.CHARACTER_BACKGROUND_DICTIONARY[pc_background])

    #then do racial proficiencies. kludgy, but with various special cases it's
    #hard to represent as a dictionary or other data structure in a way that's elegant/human-readable
    race = pc_race[0]
    subrace = pc_race[1]

    #handling this before half-elf to remove the possibility of sample() not having
    #two elements to choose from
    if pc_subclass == 'Knowledge Domain':
        domain_skill_choices = ['Arcana', 'History', 'Nature', 'Religion']
        #remove any already gained due to background, then choose 2.
        for skill in pc_proficiencies:
            if skill in domain_skill_choices:
                domain_skill_choices.remove(skill)
        pc_proficiencies.extend(sample(domain_skill_choices, 2))
    if race == 'Elf' and 'Perception' not in pc_proficiencies:
        pc_proficiencies.append('Perception')
    if race == 'Half-Elf':
        #bard list is the entire skill list
        wild_skill_choices =  list(dnd5e.CLASS_SKILLS_DICTIONARY['Bard'][1])
        #remove any already gained due to background, then choose 2.
        for skill in pc_proficiencies:
            if skill in wild_skill_choices:
                wild_skill_choices.remove(skill)
        pc_proficiencies.extend(sample(wild_skill_choices, 2))
    if race == 'Half-Orc' and 'Intimidation' not in pc_proficiencies:
        pc_proficiencies.append('Intimidation')


    #make copy of class skills
    #dictionary value fetched is a nested tuple of form (integer, (inner tuple)), where:
    #integer is the number of skills to choose
    #inner tuple is the class skill list
    class_skills = list(dnd5e.CLASS_SKILLS_DICTIONARY[pc_class][1])

    #delete overlapping background skills
    for skill in pc_proficiencies:
        if skill in class_skills:
            class_skills.remove(skill)

    #choose remaining skills without replacement, append each to pc_proficiencies
    #some edge cases like "half-elf sage cleric (knowledge domain)" can run out of skills
    #hence looping instead of a simple extend(sample())
    remaining_skill_choices = dnd5e.CLASS_SKILLS_DICTIONARY[pc_class][0]
    for i in range(remaining_skill_choices):
        if class_skills: #is nonempty
            popped_skill = choice(class_skills)
            class_skills.remove(popped_skill)
            pc_proficiencies.append(popped_skill)
    pc_proficiencies.sort()

    return pc_proficiencies

# pc_class must be a string, pc_subclass may be string or None.
# currently character_level must be == 1
# spellcasting_ability_modifier must be an int.
# returns spells prepared/known. currently doesn't handle wizard spellbooks,
# just their spells prepped.
# returns a list of lists, each inner list contains the spells of a level == inner list's index
def chooseSpells(pc_class, pc_subclass, character_level, spellcasting_ability_modifier):
    assert (pc_class in dnd5e.CLASS_SPELL_DICTIONARY.keys())
    assert (isinstance(character_level, int))
    assert (isinstance(spellcasting_ability_modifier, int))
    assert (character_level == 1) #sorry, only 1st level is supported for now

    pc_spells = [[],[]]
    filtered_spells = []
    number_of_spells = getNumberOfSpells(pc_class, character_level, spellcasting_ability_modifier)


    # Nature Domain grants a bonus druid cantrip of player's choice.
    # bonus cantrips that *aren't* chosen, like Light donain's 'light' are already accounted for in dnd5e.CLERIC_SUBCLASSES
    if pc_subclass == 'Nature Domain':
        pc_spells[0].append(choice(dnd5e.CLASS_SPELL_DICTIONARY['Druid'][0]))

    for spell_level in range(2): #i.e. 0 and 1
        #first add automatically known/prepared spells
        if pc_class == 'Cleric':
            pc_spells[spell_level].extend(dnd5e.CLERIC_SUBCLASSES[pc_subclass][spell_level])
        #get class spell list, filter out the auto-known/prepared ones so they don't get selected again
        filtered_spells.clear()
        if pc_class == 'Warlock':
            filtered_spells.extend(dnd5e.WARLOCK_SUBCLASSES[pc_subclass][spell_level])
        for spell in dnd5e.CLASS_SPELL_DICTIONARY[pc_class][spell_level]:
            if spell not in pc_spells[spell_level]:
                filtered_spells.append(spell)
        # randomly select them from filtered list
        pc_spells[spell_level].extend(sample(filtered_spells, number_of_spells[spell_level]))
    return pc_spells

# Randomly selects a ranger favored enemy, returning it as a string.
def getFavoredEnemy():
    favored_enemy = choice(dnd5e.FAVORED_ENEMIES)
    if favored_enemy == 'Humanoids':
        favored_enemy = sample(dnd5e.HUMANOID_LIST, 2)
        favored_enemy.sort()
        favored_enemy = ', '.join(favored_enemy)
    return favored_enemy

#returns number of spells prepared/known for each spell level at 1st level as a list
#Does not include additional spells granted by subclass features, like cleric domain
#currently doesn't implement any character level other than 1, or spellbook=True.
def getNumberOfSpells(pc_class, character_level, spellcasting_ability_modifier, spellbook=False):

    assert(spellbook == False or pc_class == 'Wizard'),"Only wizards have a spellbook."
    assert(pc_class in dnd5e.CLASS_SPELL_DICTIONARY.keys()),"Undefined or non-casting class."
    assert(isinstance(character_level, int))
    assert(isinstance(spellcasting_ability_modifier, int))

    number_of_spells = [0,0,0,0,0,0,0,0,0,0]

    if pc_class in ('Bard', 'Druid', 'Warlock'):
        number_of_spells[0] = 2
    elif pc_class in ('Cleric', 'Wizard'):
        number_of_spells[0] = 3
    elif pc_class == 'Sorcerer':
        number_of_spells[0] = 4

    if pc_class == 'Bard':
        number_of_spells[1] = 4
    elif pc_class in ('Cleric', 'Druid', 'Wizard'):
        if spellbook == True and pc_class == 'Wizard':
            number_of_spells[1] = 6
        else:
            # "spellcasting ability modifier + character level, minimum of one spell"
            minimum_of_one = [1, spellcasting_ability_modifier + character_level]
            minimum_of_one.sort()
            number_of_spells[1] = minimum_of_one[1]
    elif pc_class in ('Sorcerer', 'Warlock'):
        number_of_spells[1] = 2
    return number_of_spells

# returns a 2-element list [race, subrace] randomly chosen from the data in dnd5e.py
# can optionally be passed a race, in which case it'll only choose a random subrace
# if race has no subraces, will return [race, race]
def getRaceAndSubrace(race=None):
    if not race:
        race = choice(dnd5e.CHARACTER_RACES)
    if race in dnd5e.SUBRACES_DICTIONARY.keys():
        subrace = choice(dnd5e.SUBRACES_DICTIONARY[race])
    else:
        subrace = race
    return [race, subrace]

# if passed class chooses a subclass at 1st level, randomly picks subclass.
# Otherwise, returns None
def getSubclass(pc_class):
    if pc_class == 'Cleric':
        pc_subclass = choice(list(dnd5e.CLERIC_SUBCLASSES.keys()))
    elif pc_class == 'Sorcerer':
        pc_subclass = choice(list(dnd5e.SORCERER_SUBCLASSES.keys()))
    elif pc_class == 'Warlock':
        pc_subclass = choice(list(dnd5e.WARLOCK_SUBCLASSES.keys()))
    else:
        pc_subclass = None
    return pc_subclass

# pc_class must be a string
# ability_scores must be an array
# returns an int, e.ge. an input of ('Wizard', [9,10,13,15,12,8]) returns 2
def getSpellcastingModifier(pc_class, ability_scores):
    if pc_class in ('Bard', 'Sorcerer', 'Warlock'):
        spell_score = ability_scores['Cha']
    elif pc_class in ('Cleric', 'Druid'):
        spell_score = ability_scores['Wis']
    elif pc_class in ('Wizard'):
        spell_score = ability_scores['Int']
    else:
        return None
    return floor((spell_score / 2)) - 5

# generates ability scores, returns them as an **list** for easy sorting/indexing
# number_of_scores siupports any positive integer, but must == 6 if rolling_method == 'array'
# rolling_method must be one of the following strings:
# 'array' -- fixed scores of [15, 14, 13, 12, 10, 8], number_of_scores must be 6.
# '4d6L1' -- for each score, roll 4d6, ignoring the lowest die.
# '3d6' -- for each score roll 3d6
# '3d6H2+10' -- for each score roll 3d6, score = lowest die + 10
def rollAbilityScores(number_of_scores, rolling_method):

    rolled_scores = []
    die_rolls = []

    if rolling_method == '4d6L1':
        for i in range(number_of_scores):
            rolled_scores.append(0)
            die_rolls.clear()
            for die in range(4):
                die_rolls.append(randint(1, 6))
            die_rolls.sort(reverse=True)
            for die in range(3):
                rolled_scores[i] += die_rolls[die]
    elif rolling_method == 'array' and number_of_scores == 6:
        rolled_scores.extend((15, 14, 13, 12, 10, 8))
    elif rolling_method == '3d6':
        for i in range(number_of_scores):
            rolled_scores.append(0)
            for die in range(3):
                rolled_scores[i] += randint(1, 6)
    elif rolling_method == '3d6H2+10':    #for knave
        for i in range(number_of_scores):
            rolled_scores.append(0)
            die_rolls.clear()
            for die in range(3):
                die_rolls.append(randint(1, 6))
            die_rolls.sort()
            rolled_scores[i] = die_rolls[0] + 10
    else:
        raise Exception
    return rolled_scores

# assignAbilityScores() takes a **list** of rolled_scores, returns an ability score **dictionary**
# if passed in_order=True, will assign them randomly.
# otherwise, will sort/assign scores based on what's recommended for the class
# i.e. the "quick-build" suggestions in the PHB
# rolled_scores must be a list
# pc_class must be a string
def assignAbilityScores(rolled_scores, pc_class, in_order=False):
    assert isinstance((rolled_scores), list)
    assert (pc_class in dnd5e.CLASS_ABILITY_SCORES.keys())

    sorted_scores = {}
    ability_score_assigned = {}
    for key in dnd5e.ABILITY_SCORES.keys():
        sorted_scores[key] = None
        ability_score_assigned[key] = False

    if not in_order:
        rolled_scores.sort() #ascending order, so pop() returns the highest scores
        for priority_score in dnd5e.CLASS_ABILITY_SCORES[pc_class]: # priority_score will either be a string or 2-string list
            if isinstance(priority_score, str):
                assert (priority_score in dnd5e.ABILITY_SCORES.keys())
                # try:
                #     index_of_priority_score = dnd5e.ABILITY_SCORES.index(priority_score)
                # except Exception:
                #     print('priority_score:')
                #     print(priority_score)
                #     raise Exception
                if ability_score_assigned[priority_score] == False:
                    sorted_scores[priority_score] = rolled_scores.pop() #assign next highest rolled score
                    ability_score_assigned[priority_score] = True
            # next recommendation is a choice of scores, e.g. Str or Dex-based fighter
            elif isinstance(priority_score, list):
                temp_list = priority_score.copy()
                # remove scores that are already assigned
                for score in temp_list:
                    if ability_score_assigned[score] == True:
                        temp_list.remove(score)
                #choose a remaining score
                if temp_list != []:
                    chosen_score = choice(temp_list)
                    sorted_scores[chosen_score] = rolled_scores.pop() #assign next highest rolled score
                    ability_score_assigned[chosen_score] = True

    # assign remaining scores randomly
    if rolled_scores:
        shuffle(rolled_scores)
        for ability_score in dnd5e.ABILITY_SCORES.keys():
            if rolled_scores and not ability_score_assigned[ability_score]:
                sorted_scores[ability_score] = rolled_scores.pop()
    return sorted_scores




# UNDER CONSTRUCTION

# this is handled this way instead of a dictionary because a subclass & background\
# might have the same name, like "Scout" or something
# def getToolProficiencies(subrace, background, pc_class, subclass=None):
#     tool_proficiencies = []
#
#     if subrace:
#
#     if background == 'Charlatan':
#         tool_proficiencies.extend(('disguise kit', 'forgery kit'))
#     if background == 'Criminal': #one gaming set, thieves' tools
#     if background == 'Entertainer': #musical instrument
#         tool_proficiencies.extend(('disguise kit', ))
#     if background == 'Folk Hero': #artisan's tools
#         tool_proficiencies.extend(('vehicles (land)', ))
#     if background == 'Guild Artisan': #one artisan's tool
#     if background == 'Hermit':
#         tool_proficiencies.append('herbalism kit')
#     if background == 'Noble': #gaming set
#     if background == 'Outlander': #musixcal instrument
#     if background == 'Sailor':
#         tool_proficiencies.extend(('navigator\'s tools', 'vehicles (water)'))
#     if background == 'Soldier': #gaming set
#         tool_proficiencies.extend(('navigator\'s tools', 'vehicles (land)'))
#     if background == 'Urchin':
#         tool_proficiencies.extend(('disguise kit', 'thieves\' tools'))
#     if pc_class == 'Druid':
#         tool_proficiencies.append('herbalism kit')
#     if pc_class == 'Bard': #3 musical instruments
#     if pc_class == 'Monk': #one instrument or artisan tool
#     if pc_class == 'Rogue':
#         tool_proficiencies.append('thieves\' tools')
#     if subclass == 'Battle Master': #one artisan's tool
#     if subclass == 'Assassin':
#         tool_proficiencies.extend(('disguise kit', 'poisoner\'s kit'))
#     return tool_proficiencies
