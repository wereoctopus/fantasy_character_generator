# fantasy_character_generator

A set of Python 3.6 scripts that generate randomly rolled characters (including name, spells, personality traits) for D&D5e and eventually (?) other systems.